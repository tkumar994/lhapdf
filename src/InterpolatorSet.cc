// -*- C++ -*-
//
// This file is part of LHAPDF
// Copyright (C) 2012-2020 The LHAPDF collaboration (see AUTHORS for details)
//
#include "LHAPDF/InterpolatorSet.h"

namespace LHAPDF {


  namespace { // Unnamed namespace
    
    // This function subtracts the ith and jth rows in the matrix, such that the ith element is eliminated from the jth row
    int _subtractMatrix(double equations[][7], int i, int j){
      double e_i = equations[i][i];
      double e_j = equations[j][i];
      for(int k=0;k<7;k++){
        equations[j][k] = equations[j][k]*e_i - equations[i][k]*e_j;
        equations[j][k] = equations[j][k]/e_i;
      }
      return 1;
    }

    // This finds the 5fth order polynomial between points x1 and x2, and returns the value of the polynomial at x
    double _solveSystem(double x,double x1, double x2, double y1, double y2, double dy1, double dy2, double ddy1, double ddy2){
      
      double equations2[6][7];
      double pow_x[6];
      pow_x[0] = 1;
      for(int i=1;i<6;i++){
        pow_x[i] = pow_x[i-1]*x;
      }
      
      double pow_x1[6];
      pow_x1[0] = 1;
      for(int i=1;i<6;i++){
        pow_x1[i] = pow_x1[i-1]*x1;
      }
      double pow_x2[6];
      pow_x2[0] = 1;
      for(int i=1;i<6;i++){
        pow_x2[i] = pow_x2[i-1]*x2;
      }

      // normal function equations
      for(int i=0;i<6;i++){
        equations2[0][i] = pow_x1[5-i];
      }

      equations2[0][6] = y1;
      for(int i=0;i<6;i++){

        equations2[1][i] = pow_x2[5-i];
      }

      equations2[1][6] = y2;


      // first derivative equations
      for(int i=0;i<6;i++){
        if (5-i-1 >=  0){

          equations2[2][i] = (5-i)*pow_x1[5-i-1];
        } else{

          equations2[2][i] = 0;
        }
      }

      equations2[2][6] = dy1;

      for(int i=0;i<6;i++){
        if (5-i-1 >= 0){

          equations2[3][i] =  (5-i)*pow_x2[5-i-1];
        } else{

          equations2[3][i] = 0;
        }
      }

      equations2[3][6] = dy2;

      // second derivative equations
      for(int i=0;i<6;i++){
        if (5-i-2 >= 0){

          equations2[4][i] =  (5-i)*(5-i-1)*pow_x1[5-i-2];
        } else{

          equations2[4][i] = 0;
        }
      }

      equations2[4][6] = ddy1;

      for(int i=0;i<6;i++){
        if (5-i-2 >= 0){

          equations2[5][i] =  (5-i)*(5-i-1)*pow_x2[5-i-2];
        } else{

          equations2[5][i] = 0;
        }
      }

      equations2[5][6] = ddy2;


      // solving the equations
      for(int i=0;i<6;i++){
        for(int j=i+1;j<6;j++){

          _subtractMatrix(equations2,i,j);
        }
      }


      for(int i=5;i>=0;i--){
        for(int j=i-1;j>=0;j--){

          _subtractMatrix(equations2,i,j);
        }
      }


      // getting the valyue of the polynomial at x
      double ans = 0;
      for(int i=0;i<6;i++){
        ans += (equations2[i][6]*pow_x[5-i])/equations2[i][i];
      }

      return ans;
    }

    // One-dimensional linear interpolation for y(x)
    inline double _interpolateLinear(const double x, const double* a, const double* x_val)	{
      assert(x >= x_val[0]);
      assert(x_val[1] >= x);

      return a[0] + ((a[1]-a[0])*(x-x_val[0]))/(x_val[1]-x_val[0]);
    }

    // Three-dimensional interpolation for y(x)
    inline double _avgInterpolator(const double x, const double* a, const double* x_val){
      
      assert(x >= x_val[1]);
      assert(x_val[2] >= x);
      
      // y(x1) and y(x2)
      double y1 = a[1];
      double y2 = a[2];

      // first derivative
      double dy1,dy2;
      double dy_[3];
      for(int i=0;i<3;i++){
        dy_[i] = (a[i+1] - a[i])/(x_val[i+1]-x_val[i]);
      }

      dy1 = ((dy_[1]*(x_val[1] - x_val[0])) + (dy_[0]*(x_val[2] - x_val[1])))/(x_val[2] - x_val[0]);


      dy2 = ((dy_[2]*(x_val[2] - x_val[1])) + (dy_[1]*(x_val[3] - x_val[2])))/(x_val[3] - x_val[1]);


      double delta_x = x_val[2] - x_val[1];
      double A,B,C,D;
      // the polynomial is of the form A(x-x1)^3 + B(x-x1)^2 + C(x-x1) + D
      D = y1;
      C = dy1;
      A = (dy2*delta_x + dy1*delta_x + 2*y1 - 2*y2)/(delta_x*delta_x*delta_x);
      B  = (3*y2 - 3*y1 - 2*dy1*delta_x - dy2*delta_x)/(delta_x*delta_x);
      double val = x - x_val[1];
      return A*val*val*val + B*val*val + C*val + D;
    }

    // Five-dimensional interpolation for y(x)
    inline double _sixPointInter(const double x, const double* a, const double* x_val){
      double y1 = a[2];
      double y2 = a[3];
      double dy1,dy2;
      double dy_[5];
      for(int i=0;i<5;i++){
        dy_[i] = (a[i+1] - a[i])/(x_val[i+1]-x_val[i]);
      }

      // first derivative
      dy1 = ((dy_[2]*(x_val[2] - x_val[1])) + (dy_[1]*(x_val[3] - x_val[2])))/(x_val[3] - x_val[1]);
      dy2 = ((dy_[3]*(x_val[3] - x_val[2])) + (dy_[2]*(x_val[4] - x_val[3])))/(x_val[4] - x_val[2]);

      // second derivative
      double ddy1, ddy2;
      double dd_x1,dd_x3;

      dd_x1 = (2*((dy_[1]) - (dy_[0])))/(x_val[2] - x_val[0]);
      dd_x3 = (2*((dy_[3]) - (dy_[2])))/(x_val[4] - x_val[2]);
      ddy1 = ((dd_x1*(x_val[3]-x_val[2]))+(dd_x3*(x_val[2]-x_val[1])))/(x_val[3]-x_val[1]);


      double dd_x2,dd_x4;
      dd_x4 = (2*((dy_[4]) - (dy_[3])))/((x_val[5] - x_val[3]));
      dd_x2 = (2*((dy_[2]) - (dy_[1])))/(x_val[3] - x_val[1]);
      ddy2 = ( (dd_x2*(x_val[4]-x_val[3])) + (dd_x4*(x_val[3]-x_val[2])) )/(x_val[4]-x_val[2]);

      // solving the system of equations and returning the final value.
      return _solveSystem(x,x_val[2],x_val[3],y1,y2,dy1,dy2,ddy1,ddy2);
    }

    inline double _fourPointInter(const double x, const double* a, const double* x_val){

      double y1 = a[1];
      double y2 = a[2];
      
      // first derivative values
      double dy1,dy2;
      double dy1_1 = (a[1] - a[0]) / (x_val[1] - x_val[0]);
      double dy1_2 = (a[2] - a[1]) / (x_val[2] - x_val[1]);
      dy1 = ((dy1_2*(x_val[1] - x_val[0]))/(x_val[2] - x_val[0])) + ((dy1_1*(x_val[2] - x_val[1]))/(x_val[2] - x_val[0]));

      double dy2_2 = (a[3] - a[2]) / (x_val[3] - x_val[2]);
      double dy2_1 = (a[2] - a[1]) / (x_val[2] - x_val[1]);
      dy2 = ((dy2_2*(x_val[2] - x_val[1]))/(x_val[3] - x_val[1])) + ((dy2_1*(x_val[3] - x_val[2]))/(x_val[3] - x_val[1]));

      // second derivative values
      double ddy1, ddy2;

      ddy1 = 2*(((a[2] - a[1])/(x_val[2] - x_val[1])) - ((a[1] - a[0])/(x_val[1] - x_val[0])))/(x_val[2] - x_val[0]);
      ddy2 = 2*(((a[3] - a[2])/(x_val[3] - x_val[2])) - ((a[2] - a[1])/(x_val[2] - x_val[1])))/(x_val[3] - x_val[1]);

      // solving system of equations and returning interpolated value
      return _solveSystem(x,x_val[1],x_val[2],y1,y2,dy1,dy2,ddy1,ddy2);
    }

  }

  

  double InterpolatorSet::_interpolateXQ2(const KnotArray1F& subgrid, double x, size_t ix, double q2, size_t iq2) const {
    
    if (subgrid.logxs().size() < 2)
      throw GridError("PDF subgrids are required to have at least 2 x-knots for use with InterpolatorSet");
    if (subgrid.logq2s().size() < 2)
      throw GridError("PDF subgrids are required to have at least 2 Q2-knots for use with InterpolatorSet");
    


    const size_t nxknots = subgrid.xsize();
    const size_t nq2knots = subgrid.q2size();
    

    const double logx = log(x);
    const double logq2 = log(q2);


    Mode interpolate_x;
    Mode interpolate_q;
    // before interpolating, we will determine what kind of interpolation must be done in both directions
    // first we find what interpolation we do in x
    if (mode == POLY1LINEAR || ix == 0 || ix+1 == nxknots-1){
      interpolate_x = POLY1LINEAR;
      
    } else if (mode == POLY3AVERAGE || ix-1 == 0 || ix+2 == nxknots-1){
      interpolate_x = POLY3AVERAGE;      
      
    } else if (mode == POLY5SIXPOINT){
      interpolate_x = POLY5SIXPOINT;
    } else{
      interpolate_x = POLY5FOURPOINT;
    }
    

    int value_size;
    int q_start_index;
    // find what interpolation we need to do in q
    if (mode == POLY1LINEAR || iq2 == 0 || iq2+1 == nq2knots-1){
      interpolate_q = POLY1LINEAR;
      value_size = 2;
      q_start_index =iq2;
    } else if (mode == POLY3AVERAGE || iq2-1 == 0 || iq2 + 2 == nq2knots -1){
      interpolate_q =POLY3AVERAGE;
      value_size = 4;
      q_start_index = iq2 - 1;
    } else if (mode == POLY5SIXPOINT){
      interpolate_q = POLY5SIXPOINT;
      value_size = 6;
      q_start_index = iq2 - 2;
    } else{
      interpolate_q = POLY5FOURPOINT;
      value_size = 4;
      q_start_index = iq2 - 1;
    }
    


    double values[6];

    // we interpolate in x
    if (interpolate_x == POLY1LINEAR){
      // we use linear interpolator in this case
      double x_val[2] = {subgrid.logxs()[ix], subgrid.logxs()[ix+1]};
      for(int i=0;i<value_size;i++){
        double vals[2];
        vals[0] = subgrid.xf(ix,q_start_index + i);
        vals[1] = subgrid.xf(ix+1,q_start_index + i);
        values[i] = _interpolateLinear(logx,vals,x_val);
      }
    } else if (interpolate_x == POLY3AVERAGE){
      // we use 3-d avg_interpolator
      double x_val[4];
      for(int j=0;j<4;j++){
        x_val[j] = subgrid.logxs()[ix-1+j];
      }
      for(int i=0;i<value_size;i++){
        double vals[4];
        for(int j=0;j<4;j++){
          vals[j] = subgrid.xf(ix-1+j,q_start_index + i);
        }
        values[i] = _avgInterpolator(logx,vals,x_val);
      }
    } else if (interpolate_x == POLY5SIXPOINT){
      double x_val[6];
      for(int j=0;j<6;j++){
        x_val[j] = subgrid.logxs()[ix-2+j];
      }
      for(int i=0;i<value_size;i++){
        double vals[6];
        for(int j=0;j<6;j++){
          vals[j] = subgrid.xf(ix-2+j,q_start_index + i);
        }
        values[i] = _sixPointInter(logx,vals,x_val);
      }
    } else{
      // else we use four point inter
      double x_val[4];
      for(int j=0;j<4;j++){
        x_val[j] = subgrid.logxs()[ix-1+j];
      }
      for(int i=0;i<value_size;i++){
        double vals[4];
        for(int j=0;j<4;j++){
          vals[j] = subgrid.xf(ix-1+j,q_start_index + i);
        }
        values[i] = _fourPointInter(logx,vals,x_val);
      }
    }


    if (interpolate_q == POLY1LINEAR){
      double q_val[2] = {subgrid.logq2s()[iq2], subgrid.logq2s()[iq2+1]};
      return _interpolateLinear(logq2, values, q_val);
    } else if (interpolate_q == POLY3AVERAGE){
      double q_val[4];
      for(int j=0;j<4;j++){
        q_val[j] = subgrid.logq2s()[q_start_index+j];
      }
      return _avgInterpolator(logq2, values, q_val);
    } else if (interpolate_q == POLY5SIXPOINT){
      double q_val[6];
      for(int j=0;j<6;j++){
        q_val[j] = subgrid.logq2s()[q_start_index+j];
      }
      return _sixPointInter(logq2, values, q_val);
    } else{
      double q_val[4];
      for(int j=0;j<4;j++){
        q_val[j] = subgrid.logq2s()[q_start_index+j];
      }
      return _fourPointInter(logq2, values, q_val);
    }


  }


}
