// -*- C++ -*-
//
// This file is part of LHAPDF
// Copyright (C) 2012-2020 The LHAPDF collaboration (see AUTHORS for details)
//
#pragma once
#ifndef LHAPDF_InterpolatorSet_H
#define LHAPDF_InterpolatorSet_H

#include "LHAPDF/Interpolator.h"

namespace LHAPDF {

  namespace{

    enum Mode{
      POLY1LINEAR = 0,
      POLY3AVERAGE = 1,
      POLY5SIXPOINT =2,
      POLY5FOURPOINT =3
    };
  }

  /// @brief Implementation of a set of interpolators.
  ///
  /// This class will interpolate in 2D using one of three interpolators.
  class InterpolatorSet : public Interpolator {
  public:

    /// setting the Interpolator use case.
    InterpolatorSet(){
        mode = POLY3AVERAGE;
    };
    InterpolatorSet(Mode x){
        mode = x;
    };

    /// Implementation of (x,Q2) interpolation.
    double _interpolateXQ2(const KnotArray1F& subgrid, double x, size_t ix, double q2, size_t iq2) const;
    
  private:
    /// the variable that stores which interpolator to use.
    Mode mode;
  };


}
#endif
